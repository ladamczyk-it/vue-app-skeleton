module.exports = {
	root: true,
	env: {
		es6: true,
		node: true,
		browser: true,
	},
	extends: [
		'plugin:compat/recommended',
		'plugin:import/errors',
		'plugin:import/typescript',
		'plugin:sonarjs/recommended',
		'plugin:vue/recommended',
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'@vue/typescript/recommended',
		'@vue/prettier',
		'@vue/prettier/@typescript-eslint',
	],
	rules: {
		'prettier/prettier': ['error'],
		'no-console': 'error',
		'no-debugger': 'error',
		'import/no-cycle': 0,
		'import/prefer-default-export': 0,
		'@typescript-eslint/no-explicit-any': 0,
	},
	overrides: [
		{
			files: ['**/*.spec.ts'],
			rules: {
				'global-require': 0,
			},
		},
	],
	settings: {
		'import/resolver': {
			typescript: {
				alwaysTryTypes: true,
			},
		},
	},
	parserOptions: {
		parser: '@typescript-eslint/parser',
		ecmaVersion: 2020,
	},
};
