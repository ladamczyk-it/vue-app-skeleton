import Vue from 'vue';
import EventBus from './eventBus';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';
import styles from './styles';
import wait from './wait';
// import socket from './socket';
import './registerServiceWorker';

styles.install(Vue);

// this should be uncommented for WS support
// socket.install(Vue);

Vue.config.productionTip = process.env.NODE_ENV !== 'production';
Vue.config.performance = process.env.NODE_ENV !== 'production';

new Vue({
	router,
	store,
	i18n,
	wait,
	render: (h) => h(App),
}).$mount('#app');

EventBus.$emit('app-mounted');
