import { AxiosRequestConfig, AxiosResponse } from 'axios';

export default class ApiException extends Error {
	public readonly status: number;

	public readonly data: any;

	public readonly headers: any;

	public readonly config: AxiosRequestConfig;

	public readonly request: any;

	constructor(response: AxiosResponse | any) {
		super(response.statusText);

		this.status = response.status;

		if (response.data) {
			this.data = response.data;
		}

		if (response.headers) {
			this.headers = response.headers;
		}

		if (response.config) {
			this.config = response.config;
		}

		if (response.request) {
			this.request = response.request;
		}

		// Set the prototype explicitly.
		Object.setPrototypeOf(this, ApiException.prototype);
	}
}
