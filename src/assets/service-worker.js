/* eslint-disable no-restricted-globals */

/* eslint-disable-next-line no-undef */
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

self.addEventListener('push', (event) => {
	const data = event.data.json();

	self.registration.showNotification(data.title, {
		body: data.body,
	});
});

addEventListener('message', (event) => {
	const replyPort = event.ports[0];
	const message = event.data;

	if (replyPort && message && message.type === 'skip-waiting') {
		event.waitUntil(
			self.skipWaiting().then(
				() =>
					replyPort.postMessage({
						error: null,
					}),
				(error) =>
					replyPort.postMessage({
						error,
					}),
			),
		);
	}
});
/* eslint-enable no-restricted-globals */
