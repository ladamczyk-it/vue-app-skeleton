export interface Dictionary<T> {
	[key: string]: T;
}

export interface MessageInterface {
	id?: string;
	type: string;
	content: string;
	expire?: number;
	dismissible?: boolean;
	params?: Dictionary<any>;
}
