import { clone, omit } from '@/helpers/General';

describe('GeneralHelper', () => {
	it('clone', async () => {
		const sampleObject = {
			foo: 1,
		};

		const clonedObject = clone(sampleObject);

		expect(sampleObject).not.toBe(clonedObject);
		expect(sampleObject).toStrictEqual(clonedObject);
	});

	it('omit', () => {
		const object = {
			foo: 1,
			bar: 2,
			remove: 3,
		};
		const omittedObject = {
			foo: 1,
			bar: 2,
		};

		expect(omit(object, ['remove'])).toStrictEqual(omittedObject);
	});
});
