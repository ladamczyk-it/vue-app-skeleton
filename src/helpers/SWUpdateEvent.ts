export default class SWUpdateEvent {
	constructor(private readonly _registration: ServiceWorkerRegistration) {
		// dummy
	}

	/**
	 * Check if the new service worker exists or not.
	 */
	update(): Promise<void> {
		return this._registration.update();
	}

	/**
	 * Activate new service worker to work 'location.reload()' with new data.
	 */
	skipWaiting(): Promise<void> {
		const worker = this._registration.waiting;

		if (!worker) {
			return Promise.resolve();
		}

		return new Promise((resolve, reject) => {
			const channel = new MessageChannel();

			channel.port1.onmessage = (event) => {
				if (event.data.error) {
					reject(event.data.error);
				} else {
					resolve(event.data);
				}
			};

			worker.postMessage({ type: 'skip-waiting' }, [channel.port2]);
		});
	}
}
