import merge from 'lodash/merge';
import isArray from 'lodash/isArray';

export const clone = <T>(object: T): T => {
	try {
		return JSON.parse(JSON.stringify(object));
	} catch (e) {
		return merge(isArray(object) ? [] : {}, object);
	}
};

export const omit = <T, K extends keyof T>(object: T, keys: string[]): Omit<T, K> => {
	const omitSingle = <W, E extends string>(o: W, k: E): Omit<W, E> => {
		/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
		const { [k]: omitted, ...rest } = o;

		return rest;
	};
	let omittedObject = clone(object);

	keys.forEach((key) => {
		omittedObject = omitSingle(omittedObject, key) as T;
	});

	return omittedObject;
};
