import { GetterTree } from 'vuex';
import { StoreGetters } from '@/store/constants';
import { MessageInterface } from '@/types';
import { RootState } from './types';

export const getters: GetterTree<RootState, RootState> = {
	[StoreGetters.GET_MESSAGES]: (state): MessageInterface[] => state.messages,
};
