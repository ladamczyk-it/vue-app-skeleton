import { ActionTree } from 'vuex';
import { StoreActions, StoreMutations } from '@/store/constants';
import { MessageInterface } from '@/types';
import md5 from 'md5';
import { RootState } from './types';

export const actions: ActionTree<RootState, RootState> = {
	[StoreActions.ADD_TOAST]: ({ commit }, message: MessageInterface) => {
		const id = md5(JSON.stringify(message));

		commit(StoreMutations.PUSH_MESSAGE, {
			id,
			...message,
		});
	},
};
