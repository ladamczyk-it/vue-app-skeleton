import { MutationTree } from 'vuex';
import md5 from 'md5';
import { StoreMutations } from '@/store/constants';
import { MessageInterface } from '@/types';
import { RootState } from './types';

export const mutations: MutationTree<RootState> = {
	[StoreMutations.PUSH_MESSAGE]: (state, message: MessageInterface) => {
		if (!message.id) {
			message.id = String(md5(JSON.stringify(message)));
		}

		if (!state.messages.find((item) => item.id === message.id)) {
			state.messages.push(message);
		}
	},
	[StoreMutations.POP_MESSAGE]: (state, messageId: string) => {
		state.messages = state.messages.filter((message) => message.id !== messageId);
	},
};
