import Vue from 'vue';
import Vuex from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { auth } from './auth';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		language: (localStorage && localStorage.getItem('language')) || process.env.VUE_APP_DEFAULT_LANGUAGE,
		messages: [],
	},
	mutations,
	getters,
	modules: {
		auth,
	},
	actions,
	strict: true,
	devtools: true,
});
