import { ActionTree } from 'vuex';
import { StoreActions, StoreMutations } from '@/store/constants';
import AuthService from '@/services/AuthService';
import { CredentialsInterface, UserAuthInterface } from '@/types/services/Auth';
import { AxiosResponse } from 'axios';
import { AuthState } from './types';
import { RootState } from '../types';

export const actions: ActionTree<AuthState, RootState> = {
	[StoreActions.AUTH_LOGIN]: ({ commit }, credentials: CredentialsInterface) =>
		new AuthService().login(credentials).then((response: AxiosResponse<UserAuthInterface>) => {
			const { data } = response;

			commit(StoreMutations.AUTH_SET_USER, data);
			commit(StoreMutations.SET_API_TOKEN, data.access_token);
		}),
	[StoreActions.AUTH_LOGOUT]: ({ commit }) => {
		commit(StoreMutations.SET_API_TOKEN, '');
		commit(StoreMutations.AUTH_SET_USER, null);
	},
};
