import { Dictionary } from '@/types';

export const noop = (): Dictionary<any> => {
	return {};
};

export const emptyArray = (): any[] => [];

export const Languages: Dictionary<string> = {
	en: 'English',
};

export enum MessageType {
	SUCCESS = 'success',
	WARNING = 'warning',
	ERROR = 'danger',
	INFO = 'info',
}

export const GENERAL_ERROR_MESSAGE = {
	type: MessageType.ERROR,
	content: 'errors.general',
};
