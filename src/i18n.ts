import Vue from 'vue';
import VueI18n from 'vue-i18n';
import { Languages } from '@/config';
import { Dictionary } from '@/types';

Vue.use(VueI18n);
const messages: Dictionary<any> = {};

Object.keys(Languages).forEach((key) => {
	messages[key] = require(`@/translations/${key}.json`);
});

export default new VueI18n({
	locale: localStorage && localStorage.getItem('language') ? localStorage.getItem('language') : process.env.VUE_APP_DEFAULT_LANGUAGE,
	messages,
	silentTranslationWarn: true,
});
