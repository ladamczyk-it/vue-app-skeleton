import { Wrapper } from '@vue/test-utils';

export const isRendered = (wrapper: Wrapper<any>, selector: string, title = 'Is rendered?'): void =>
	test(title, () => {
		expect(wrapper.find(selector).exists()).toBe(true);
	});
