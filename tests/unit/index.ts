import Vue from 'vue';
import styles from '@/styles';
import { Dictionary } from '@/types';

styles.install(Vue);

/* eslint-disable-next-line func-names */
const localStorageMock = (function () {
	let store: Dictionary<string> = {};

	return {
		getItem(key: string) {
			return store[key] || null;
		},
		setItem(key: string, value: string) {
			store[key] = value.toString();
		},
		clear() {
			store = {};
		},
	};
})();

Object.defineProperty(window, 'localStorage', {
	value: localStorageMock,
});

/* eslint-disable-next-line func-names */
const cacheApiMock = (function () {
	return {
		open(key: string) {
			return new Promise((resolve) => {
				resolve(key);
			});
		},
	};
})();

Object.defineProperty(window, 'caches', {
	value: cacheApiMock,
});
