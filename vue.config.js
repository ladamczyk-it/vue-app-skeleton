module.exports = {
	lintOnSave: false,
	devServer: {
		disableHostCheck: true,
		watchOptions: {
			ignored: /node_modules/,
			aggregateTimeout: 300,
			poll: 1000,
		},
	},
	css: {
		sourceMap: true,
		loaderOptions: {
			sass: {
				additionalData: '@import "@/assets/sass/_core.scss";',
			},
		},
	},
	pwa: {
		themeColor: '#007BFF',
		msTileColor: '#000000',
		workboxPluginMode: 'InjectManifest',
		workboxOptions: {
			swSrc: './src/assets/service-worker.js',
		},
	},
};
